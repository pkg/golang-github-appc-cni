golang-github-appc-cni (1.1.2-1+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Sat, 01 Apr 2023 00:58:23 +0000

golang-github-appc-cni (1.1.2-1) unstable; urgency=medium

  * Team upload
  * New upstream version 1.1.2
  * Update Standards-Version to 4.6.1 (no changes)
  * Remove old appc cni link
  * Downgrade to ginkgo v1

 -- Shengjing Zhu <zhsj@debian.org>  Sat, 20 Aug 2022 21:02:47 +0800

golang-github-appc-cni (1.0.1-3) unstable; urgency=medium

  * Team upload
  * Upload to unstable

 -- Shengjing Zhu <zhsj@debian.org>  Mon, 21 Feb 2022 22:47:39 +0800

golang-github-appc-cni (1.0.1-2) experimental; urgency=medium

  * Team upload
  * Add patches to keep some old interfaces

 -- Shengjing Zhu <zhsj@debian.org>  Sun, 10 Oct 2021 22:08:02 +0800

golang-github-appc-cni (1.0.1-1) experimental; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Remove 1 maintscript entries from 1 files.

  [ Shengjing Zhu ]
  * New upstream version 1.0.1

 -- Shengjing Zhu <zhsj@debian.org>  Sun, 10 Oct 2021 20:08:15 +0800

golang-github-appc-cni (0.8.1-1apertis0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Thu, 15 Apr 2021 14:19:41 +0200

golang-github-appc-cni (0.8.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 0.8.1
    + Tighten up plugin-finding logic (Closes: #983659)
      CVE-2021-20206
  * Update Section to golang
  * Update maintainer address to team+pkg-go@tracker.debian.org
  * Bump debhelper-compat to 13
  * Add Rules-Requires-Root
  * Add Multi-Arch hint
  * Skip integration test
  * Replace golang-ginkgo-dev with golang-github-onsi-ginkgo-dev
  * Update Standards-Version to 4.5.1 (no changes)

 -- Shengjing Zhu <zhsj@debian.org>  Sun, 28 Feb 2021 19:20:44 +0800

golang-github-appc-cni (0.8.0-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 19 Jul 2020 14:23:28 -0400

golang-github-appc-cni (0.8.0-1) experimental; urgency=medium

  * New upstream release.
  * Upload to experimental (API break breaks libpod)

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 12 Jul 2020 09:39:44 -0400

golang-github-appc-cni (0.7.1-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Smirnov <onlyjob@debian.org>  Thu, 17 Oct 2019 02:45:52 +1100

golang-github-appc-cni (0.7.1-1) experimental; urgency=medium

  * New upstream release.
  * Standards-Version: 4.4.0.
  * DH & compat to version 12.
  * golang-go --> golang-any.
  * (Build-)Depends:
    - golang-github-coreos-go-iptables-dev
    - golang-github-coreos-go-systemd-dev
    - golang-github-d2g-dhcp4client-dev
    - golang-github-d2g-dhcp4-dev
    - golang-github-vishvananda-netlink-dev

 -- Dmitry Smirnov <onlyjob@debian.org>  Thu, 12 Sep 2019 11:22:53 +1000

golang-github-appc-cni (0.7.0~rc2-1) experimental; urgency=medium

  * Team Upload
  * New upstream release

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 03 Mar 2019 14:16:15 -0500

golang-github-appc-cni (0.4.0+dfsg-1) unstable; urgency=medium

  [ Alexandre Viau ]
  * Point Vcs-* urls to salsa.debian.org.

  [ Dmitry Smirnov ]
  * New upstream release [January 2017].
  * New upstream patch to build with go-systemd v17.
  * Testsuite: autopkgtest-pkg-go.
  * Standards-Version: 4.2.0; Priority: optional.
  * debhelper & compat to version 11.

 -- Dmitry Smirnov <onlyjob@debian.org>  Fri, 17 Aug 2018 21:57:39 +1000

golang-github-appc-cni (0.3.0+dfsg-1) unstable; urgency=medium

  * New upstream release [June 2016].
  * New name space, home page and compatibility symlink.
  * Provides: golang-github-containernetworking-cni-dev

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 19 Jun 2016 19:50:55 +1000

golang-github-appc-cni (0.2.3+dfsg-1) unstable; urgency=medium

  * New upstream release [May 2016].
  * Standards-Version: 3.9.8.
  * Vcs-Git URL to HTTPS.

 -- Dmitry Smirnov <onlyjob@debian.org>  Sat, 21 May 2016 21:09:50 +1000

golang-github-appc-cni (0.2.0~rc0+dfsg-1) unstable; urgency=medium

  * New upstream pre-release.
  * Build-Depends:
    + golang-ginkgo-dev
    + golang-gomega-dev
  * Upgraded watch file to version 4.

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 03 Apr 2016 17:37:28 +1000

golang-github-appc-cni (0.1.0+dfsg-1) unstable; urgency=medium

  * Initial release (Closes: #818945).

 -- Dmitry Smirnov <onlyjob@debian.org>  Tue, 22 Mar 2016 11:35:11 +1100
